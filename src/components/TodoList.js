import React from 'react';
import Todolistitem from './TodoListItem';
import './TodoListItem.scss';

const Todolist = ({ todos, onRemove, onToggle }) => {
  return (
    <div className="TodoList">
      {todos.map((todo) => (
        <Todolistitem todo={todo} key={todo.id} onRemove={onRemove} onToggle={onToggle}/>
      ))}
    </div>
  );
};

export default Todolist;
